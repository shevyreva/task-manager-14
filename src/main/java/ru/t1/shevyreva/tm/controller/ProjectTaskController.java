package ru.t1.shevyreva.tm.controller;

import ru.t1.shevyreva.tm.api.controller.IProjectTaskController;
import ru.t1.shevyreva.tm.api.service.IProjectTaskService;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectId, taskId);
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectId, taskId);
        System.out.println("[OK]");
    }

}

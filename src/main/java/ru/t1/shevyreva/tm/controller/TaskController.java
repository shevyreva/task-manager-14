package ru.t1.shevyreva.tm.controller;

import ru.t1.shevyreva.tm.api.controller.ITaskController;
import ru.t1.shevyreva.tm.api.service.IProjectTaskService;
import ru.t1.shevyreva.tm.api.service.ITaskService;
import ru.t1.shevyreva.tm.enumerated.Sort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    public TaskController(final ITaskService taskService, IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = taskService.findAll(sort);
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    public void showRemoveById() {
        System.out.println("[REMOVE BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
        System.out.println("[OK]");
    }

    public void showRemoveByIndex() {
        System.out.println("[REMOVE BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.removeByIndex(index);
        System.out.println("[OK]");
    }

    public void showUpdateById() {
        System.out.println("[UPDATE BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        final String description = TerminalUtil.nextLine();
        taskService.updateById(id, name, description);
        System.out.println("[OK]");
    }

    public void showUpdateByIndex() {
        System.out.println("[UPDATE BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        final String description = TerminalUtil.nextLine();
        taskService.updateByIndex(index, name, description);
        System.out.println("[OK]");
    }

    public void showByIndex() {
        System.out.println("[SHOW BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    public void showById() {
        System.out.println("[SHOW BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("[TASK ID]: " + task.getId());
        System.out.println("[TASK NAME]: " + task.getName());
        System.out.println("[TASK DESCRIPTION]: " + task.getDescription());
        System.out.println("[TASK STATUS]: " + Status.toName(task.getStatus()));
        System.out.println("[SHOW PROJECT ID]: " + task.getProjectId());
    }

    public void showTasksByProjectId() {
        System.out.println("[SHOW ALL TASKS BY PROJECT ID]");
        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findAllTaskByProjectId(projectId);
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatusById(id, status);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeTaskStatusByIndex(index, status);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    public void startTaskStatusByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    public void startTaskStatusById() {
        System.out.println("[START TASK BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    public void completeTaskStatusByIndex() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    public void completeTaskStatusById() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusById(id, Status.COMPLETED);
        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

}

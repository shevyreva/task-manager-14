package ru.t1.shevyreva.tm.enumerated;

public enum Status {

    NON_STARTED("Non Started"),
    IN_PROGRESS("In Progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

}

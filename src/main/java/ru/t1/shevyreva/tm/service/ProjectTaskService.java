package ru.t1.shevyreva.tm.service;

import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.service.IProjectTaskService;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return;
        task.setProjectId(null);
    }

    public void removeByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.remove(task);
        projectRepository.removeById(projectId);
    }

    public List<Task> findAllTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

}

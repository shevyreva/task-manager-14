package ru.t1.shevyreva.tm.repository;

import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        final List<Project> result = new ArrayList<>(projects);
        result.sort(comparator);
        return result;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    public void remove(final Project project) {
        projects.remove(project);
    }

    public void removeById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return;
        remove(project);
    }

    public void removeByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return;
        remove(project);
    }

    public boolean existsById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return false;
        else return true;
    }

}

package ru.t1.shevyreva.tm.api.model;

import ru.t1.shevyreva.tm.enumerated.Status;

public interface IHaveStatus {

    Status getStatus();

    void setStatus(Status status);

}

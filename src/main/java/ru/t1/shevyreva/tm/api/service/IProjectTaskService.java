package ru.t1.shevyreva.tm.api.service;

import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void unbindTaskToProject(String projectId, String taskId);

    void removeByProjectId(String projectId);

    List<Task> findAllTaskByProjectId(String projectId);

}

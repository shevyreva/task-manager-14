package ru.t1.shevyreva.tm.api.controller;

public interface ICommandController {

    void showCommandError();

    void showArgumentError();

    void showAbout();

    void showVersion();

    void showInfo();

    void showCommand();

    void showArgument();

    void showHelp();

}
